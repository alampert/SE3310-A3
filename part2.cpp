// Andrew Lampert
// 250 643 797
// SE3310 - Assignment 3 - Question

#include <iostream>
#include <vector>		// Handling lists


// Classes
#include "Transition.h"
#include "TuringMachine.h"

using namespace std;

int main(int argc, char* argv[]){

	try {

		TuringMachine	tm 	= TuringMachine();

		string filePath;

		if( argc != 2 ){
			cout << "Please make sure that you remove the '<' between the command call, and the input file" << endl;
			cout << "Defaulting to input.txt" << endl;
			filePath = "input.txt";
		} else {
			filePath = argv[1];
		}

		tm.readFile(filePath);

		// tm.understanding();

		for( int i = 0; i < tm.tape_count(); i++){
			Tape t = tm.tapes()[i];
			cout << "Running for tape: " << i << endl;
			tm.run(t);
		}

		// tm.test_tape();


	} catch(int n) {
		cout << endl << endl;

		string err_msg;

		switch(n){
			case 1: err_msg = "Did not accept. Please make sure there are no spaces or blank lines in the input.txt file.";
					break;
			case 2: err_msg = "Exception was thrown. File was not opened";
					break;
			case 3: err_msg = "input / ouput character length is not 1";
					break;
			case 4: err_msg = "Type mismatch. Loading incorrect data type for TM";
					break;
			case 5: err_msg = "Incompatable execution. Format: ./{Program} {Input File} > output.txt";
					break;
			default: err_msg = "Unknown Error Message";
					break;
		}

		cout << "------------ EXCEPTION THROWN ------------" << endl;
		cout << "Case: " << n << endl;
		cout << err_msg;

		cout << endl<< endl;
	}

	return 0;
}
#include "Tape.h"
#include <fstream>
#include <sstream>
#include "myFuncs.h"

Tape::Tape(){
	m_tape = vector<char>();
	m_tape_pos = 0;
}

void Tape::loadTape(string line){

	if( !line[0] == 'i' ){
		throw 4;
	}

	for (int i = 2; i < line.length(); i++){

		this->m_tape.push_back(line[i]);

	}
}

void Tape::display(){

	string results = "";
	string tmp;
	stringstream ss;

	for( int i = 0; i < m_tape.size(); i++){

		results += m_tape[i];
	}

	cout << results << endl;
}

char Tape::readTape(){
	return m_tape[m_tape_pos];
}

void Tape::moveHead(int amt){
	m_tape_pos += amt;
}

void Tape::test(){
	cout << "TEST: " << m_tape[100] << endl;
}

void Tape::write(char c){
	m_tape[m_tape_pos] = c;
}
#include "TuringMachine.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cmath>
#include "myFuncs.h"

TuringMachine::TuringMachine(){
	m_transitions = vector<Transition>();
	m_final_states = vector<int>();
	m_tapes = vector<Tape>();
	m_current_state = 0;
}

void TuringMachine::readFile(string file_path){

	// Variables
	string				line;
	fstream				instructionFile;	// Provides transitions, accept states, and input tape.

	instructionFile.open( file_path.c_str() );

	// Error handling in case file did not open properly.
	if( !instructionFile.is_open() ){
		throw 2;
	}


	// Loading Tags

	while ( getline (instructionFile, line) ) {

		// Reading lines based on first character

		if( line[0] == 't' )		readTransition(line);
		else if (line[0] == 'f') 	readFinalStates(line);
		else if (line[0] == 'i')	readInputTape(line);

	}

	instructionFile.close();
}

void TuringMachine::understanding(){
	for( unsigned i = 0; i < m_transitions.size(); i++){
		cout << "Understood Transitions: " << m_transitions[i].display() << endl;
		m_transitions[i].display_long();
		cout << endl << endl;
	}

	cout << "Understood final states: ";
	for( unsigned i = 0; i < m_final_states.size(); i++){
		cout << m_final_states[i] << ", " ;
	}
	cout << endl;

	cout << "Understood Tape: ";
	for( unsigned i = 0; i < m_tapes.size(); i++){
		m_tapes[i].display();
	}
	cout << endl;
}

void TuringMachine::readTransition(string line){

	// Confirming that the line is a transition line
	if( !line[0] == 't' ){
		throw 4;
	}

	istringstream iss(line);
	string s;

	string current_state, input_char, next_state, output_char, direction;

	int s_index = 0;

	while ( getline( iss, s, ' ' ) ){

		switch(s_index){

			// case 0 is the t
			case 1: current_state = s;
			case 2: input_char = s;
			case 3: next_state = s;
			case 4: output_char = s;
			case 5: direction = s;

		}

		s_index++;
	}

	Transition t = Transition(current_state, input_char, next_state, output_char, direction);

	m_transitions.push_back(t);
}

void TuringMachine::readFinalStates(string line){

	if( !line[0] == 'f' ){
		throw 4;
	}

	istringstream iss(line);
	string s;
	int s_index = 0;		// To skip first char, 'f', when adding final states

	while ( getline( iss, s, ' ' ) ){

		if(s_index > 0) m_final_states.push_back(str_to_int(s));

		s_index++;
		
	}

}

void TuringMachine::readInputTape(string line){
	if( !line[0] == 'i' ){
		throw 4;
	}
	Tape t = Tape();
	t.loadTape(line);
	m_tapes.push_back(t);

}

void TuringMachine::run(Tape m_tape){

	
	vector<Transition> current_state_transitions;
	bool hasAccepted = false;
	bool transitionExec;

	while(!hasAccepted){

		cout << "Running Tape" << endl;
		m_tape.display();

		transitionExec = false;

		// Reading Current Character on Tape
		char read = m_tape.readTape();

		// Checking to see if current state is a final state 
		hasAccepted = isAcceptState(m_tape);

		// Getting list of transitions from current state
		current_state_transitions = getTransitionsFromState(m_current_state);

		// Moving tape head and reading character;
		for( int i = 0; i < current_state_transitions.size(); i++){

			// If the read character matches the input character
			if( read == current_state_transitions[i].input_char() ){
				transitionExec = true;
				execTransition(current_state_transitions[i], &m_tape);

			} else if ((read == NULL) && current_state_transitions[i].input_char() == '_' ){
				// if read is null and the input character is _
				transitionExec = true;
				execTransition(current_state_transitions[i], &m_tape);
			}
		}

		if(current_state_transitions.size() == 0 || !transitionExec){
			throw 1;
		}


	}

}

vector<Transition> TuringMachine::getTransitionsFromState(int state){

	vector<Transition> transitions = vector<Transition>();

	// Building list of transitions from current state
	for( int i = 0; i < m_transitions.size(); i++){
		if( m_transitions[i].current_state() == state){

			transitions.push_back(m_transitions[i]);

		}
	}

	return transitions;
}

bool TuringMachine::isAcceptState(Tape t){

	for( int i = 0; i < m_final_states.size(); i++){
		if( m_current_state == m_final_states[i] ){
			t.display();
			cout << "Accept" << endl;
			return true;
		}
	}

	return false;
}

void TuringMachine::execTransition(Transition t, Tape* tape){

	cout << "Current Tape: " << endl;
	tape.display();

	cout << "Executing transtion from state " << t.current_state() << " to " << t.next_state() << endl;
	cout << "Input Char: " << t.input_char() << endl;

	// Write to tape
	tape.write(t.output_char());

	// 0 = Left, 1 = right
	if( t.direction() == 0){
		tape.moveHead(-1);
	} else if (t.direction() == 1){
		tape.moveHead(1);
	} else {
		throw 5;
	}

	m_current_state = t.next_state();
}

void TuringMachine::test_tape(Tape tape){
	tape.test();
}

int TuringMachine::tape_count(){
	return m_tapes.size();
}

vector<Tape> TuringMachine::tapes(){
	return m_tapes;
}
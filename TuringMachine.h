#ifndef TURINGMACHINE_H
#define TURINGMACHINE_H

#include <iostream>
#include <vector>
#include <string>

#include "Transition.h"
#include "Tape.h"

using namespace std;

class TuringMachine {

	vector<Transition> m_transitions;
	vector<int> m_final_states;
	int m_current_state;
	vector<Tape> m_tapes;

	public:
		// Default Constructor
		TuringMachine();

		void readFile(string file_path);

		void understanding();				// Displays the loaded TM info

		void readTransition(string line);
		void readFinalStates(string line);
		void readInputTape(string line);

		void run(Tape tape);

		vector<Transition> getTransitionsFromState(int state);

		bool isAcceptState(Tape t);

		void execTransition(Transition t, Tape tape);

		void test_tape(Tape tape);

		int tape_count();
		vector<Tape> tapes();

};

#endif // TURINGMACHINE